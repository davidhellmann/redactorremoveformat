# Craft Redactor Remove Format

Craft Redactor Remove Format is a Plugin for Craft CMS. It removes all formattings from the selected text in redactor.

## Installation

* Place the **redactorremoveformat** folder inside your **craft/plugins/** folder.
* Go to **settings/plugins** and install

## Usage

Add 'removeformat' to the plugins array in your Redactor plugins configuration: http://buildwithcraft.com/docs/rich-text-fields#redactor-configs):

```
{
	buttons: ['html','formatting','bold','italic','link','image'],
	plugins: ['fullscreen', 'removeformat'],
    formattingTags: ['p', 'blockquote', 'pre', 'h1', 'h2', 'h3', 'h4'],
    observeLinks: true,
	toolbarFixedBox: true
}
```

## Credits

* imperavi for redactor
* stefan friedrich @ kreisvier.ch
* for any questions: friedrich@kreisvier.ch